#include <time.h>
#include <stdlib.h>

typedef void (*func)(int* mat, int len);
typedef void (*func2)(int* mat, int row, int column);

typedef void (*vmethod) (struct matrixList m); 

struct matrixList
{
	int row;
	int column;
	int* __restrict mat;
	vmethod* vtable_ptr;
	int* __restrict m2;

};
struct vectorList
{
	int len;
	int maximum;
	int minimum;
	struct matrixList vector;
};

void vectorNorm(struct matrixList m){
	int sum=0;
	for(int i=0; i<m.len; i++){
		sum = sum + m.mat[i];

	}

	printf("%d\n", sum);
}
void matrixNorm(struct matrixList m){
	int sum=0;
	int i=0;
	int j=0;
	int max =m.mat[0];
	for(i=0; i<m.row; i++){
		for(j=0;j<m.column; j++){
		
		
		if( m.mat[(i*m.row) + j]>max){
			max=m.mat[(i*m.row) + j];
			}
		}
	sum=sum+max;
	max=0;

	}

	printf("%d\n", sum);
}


void vectorInit(struct vectorList *v,int x) {
	
	v->len = x;
	v->vector.column=1;
	v->vector.row=x;
	v->vector.mat = (int*)malloc(v->len*sizeof(int));
	for (int i = 0; i<v->len; i++){
		v->vector.mat[i] = 1;
	}
	
}


void matrixInit(struct matrixList *m, int x, int y) {
		//std::cout << "Row: " << row << " Column: " << column << std::endl;
		m->row = x;
		m->column = y;
		m->mat=(int*)malloc(m->column*m->row*sizeof(int));
		
	}



int multiplyMat(struct matrixList m1, struct matrixList m2){
	int i = 0;
	int j = 0;
	int k = 0;
	if (m1.column == m2.row){
		//std::cout << "multiplication possible\n";
		int* mult = (int*)malloc(m1.row*m2.column*sizeof(int));

		for (i = 0; i<(m1.row*m2.column); ++i){
			mult[i] = 0;
		}

		for (i = 0; i<m1.row; i++){
			for (j = 0; j<m2.column; j++)
			{
				for (k = 0; k<m2.row; k++){
					//m.mat[row*column]=1;
					//std::cout << "mat[" << i+1 << "]["<< j+1 << "]: " << matrix1.mat[row*column];
					//	 std::cout << (m1.mat[(i*m1.column)+k]) << "* mat["<<(k*m2.column)+j<<"] "<<(m2.mat[(k*m2.column)+j]) <<"   ";
					mult[(i*m2.column) + j] = mult[(i*m2.column) + j] + (m1.mat[(i*m1.column) + k])* (m2.mat[(k*m2.column) + j]);

				}
				//std::cout <<mult[(i*m2.column)+j] << "   ";
				//std::cout << "\n" ;
			}
			//std::cout << "\n";


		}
	}
	else {
		//std::cout << "multiplication NOT possible\n";

	}

	return 0;
}

int addMat(struct matrixList m1, struct matrixList m2){
	int i = 0;
	int j = 0;
	int k = 0;
	if ((m1.column == m2.column) &&(m1.row==m2.row)){
		//std::cout << "multiplication possible\n";
		int* mult = (int*)malloc(m1.row*m2.column*sizeof(int));

		for (i = 0; i<(m1.row*m2.column); ++i){
			mult[i] = 0;
		}

		for (i = 0; i<m1.row*m1.column; i++){
			
					mult[i] = m1.mat[i] + m2.mat[i];
					printf("%d\n",mult[i]);
		}
	}
	else {		
		printf("Addition not possible");
		}

	return 0;
}


int multiplyVecMat(struct matrixList m, struct vectorList v){
	int i = 0;
	int j = 0;
	//printf("Here");
	if (m.column == v.vector.row){
		//printf("Here");
		int* mult = (int*)malloc(m.row*sizeof(int));		
		for (i = 0; i<m.row; ++i){
			mult[i] = 0;
			//std::cout <<mult[i] << "\n";
		}

		for (i = 0; i<m.row; ++i){
			for (j = 0; j<m.column; ++j)
			{

				//m.mat[row*column]=1;
				//std::cout << "mat[" << i+1 << "]["<< j+1 << "]: " << matrix1.mat[row*column];
				// std::cout << m.mat[row*column] << "   ";
				mult[i] = mult[i] + m.mat[i + j] * v.vector.mat[j];
				printf("%d\n",mult[i]);
				if (j == (m.column - 1)){
					//std::cout <<mult[i] << "\n";
						printf("%d\n",mult[i]);
				}
			}
			//std::cout << "\n" ;
		}



	}
	else {
		//std::cout << "multiplication NOT possible\n";

	}

	return 0;
}
int main()
{


	//g++ -o Task01 Task01.cpp -std=c++11

	int length = 5;
	int row = 5;
	int column = 5;
	int k = 0;

	length = 5;
	struct vectorList vec; 

	struct matrixList matrix1;
	struct matrixList matrix2;

	vmethod *vtable_vector_class = malloc(sizeof(vmethod)); 
	vmethod *vtable_matrix_class = malloc(sizeof(vmethod)); 
	
	matrixInit(&matrix1,row,column);
	matrixInit(&matrix2, row, column);
	vectorInit(&vec,length);
	
	vtable_vector_class[0] = vectorNorm;
	vtable_matrix_class[0] = matrixNorm;	

	vec.vector.vtable_ptr = &vtable_vector_class;
	matrix1.vtable_ptr = &vtable_matrix_class;
	int i = 0;
	int j = 0;
	for (i = 0; i<row; ++i){
		for (j = 0; j<column; ++j)
		{

			matrix1.mat[(i*row) + j] = 1;
			matrix2.mat[(i*row) + j] = 1;
			//printf("mat[%d][%d]:%d \n", i+1,j+1, matrix2.mat[(i*row)+j]);
			// std::cout << matrix1.mat[i+j] << "   ";
		}
		//std::cout << "\n" ;
		//printf("\n");
	}

	//multiplyVecMat(matrix1, vec);
	//addMat(matrix1,matrix2);
	matrix1.vtable_ptr[0](matrix1);
	//vectorNorm(vec.vector);
	//matrixNorm(matrix1);
	
	
}


